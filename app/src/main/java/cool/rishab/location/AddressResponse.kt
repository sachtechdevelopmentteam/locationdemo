package cool.rishab.location

import com.google.gson.annotations.SerializedName

data class AdditionalDatum(
    @SerializedName("value") var value: String? = null,
    @SerializedName("key") var key: String? = null
)

data class Address(
    @SerializedName("Label") var label: String? = null,
    @SerializedName("Country") var country: String? = null,
    @SerializedName("State") var state: String? = null,
    @SerializedName("County") var county: String? = null,
    @SerializedName("City") var city: String? = null,
    @SerializedName("District") var district: String? = null,
    @SerializedName("Street") var street: String? = null,
    @SerializedName("HouseNumber") var houseNumber: String? = null,
    @SerializedName("PostalCode") var postalCode: String? = null,
    @SerializedName("AdditionalData") var additionalData: List<AdditionalDatum>? = null
)

data class BottomRight(
    @SerializedName("Latitude") var latitude: Double? = null,
    @SerializedName("Longitude") var longitude: Double? = null
)

data class DisplayPosition(
    @SerializedName("Latitude") var latitude: Double? = null,
    @SerializedName("Longitude") var longitude: Double? = null
)

data class Location (
    @SerializedName("LocationId") var locationId: String? = null,
    @SerializedName("LocationType") var locationType: String? = null,
    @SerializedName("DisplayPosition") var displayPosition: DisplayPosition? = null,
    @SerializedName("NavigationPosition") var navigationPosition: List<NavigationPosition>? = null,
    @SerializedName("MapView") var mapView: MapView? = null,
    @SerializedName("Address") var address: Address? = null,
    @SerializedName("MapReference") var mapReference: MapReference? = null
)

data class LocationResponse(

    @SerializedName("Response") var response: Response? = null
)

data class MapReference(

    @SerializedName("ReferenceId") var referenceId: String? = null,
    @SerializedName("MapId") var mapId: String? = null,
    @SerializedName("MapVersion") var mapVersion: String? = null,
    @SerializedName("MapReleaseDate") var mapReleaseDate: String? = null,
    @SerializedName("Spot") var spot: Double? = null,
    @SerializedName("SideOfStreet") var sideOfStreet: String? = null,
    @SerializedName("CountryId") var countryId: String? = null,
    @SerializedName("StateId") var stateId: String? = null,
    @SerializedName("CountyId") var countyId: String? = null,
    @SerializedName("CityId") var cityId: String? = null,
    @SerializedName("BuildingId") var buildingId: String? = null,
    @SerializedName("AddressId") var addressId: String? = null
)

data class MapView(
    @SerializedName("TopLeft") var topLeft: TopLeft? = null,
    @SerializedName("BottomRight") var bottomRight: BottomRight? = null
)

data class MatchQuality(
    @SerializedName("Country") var country: Int? = null,
    @SerializedName("State") var state: Int? = null,
    @SerializedName("County") var county: Int? = null,
    @SerializedName("City") var city: Int? = null,
    @SerializedName("District") var district: Int? = null,
    @SerializedName("Street") var street: List<Int>? = null,
    @SerializedName("HouseNumber") var houseNumber: Int? = null,
    @SerializedName("PostalCode") var postalCode: Int? = null
)

data class MetaInfo(
    @SerializedName("Timestamp") var timestamp: String? = null,
    @SerializedName("NextPageInformation") var nextPageInformation: String? = null
)

data class NavigationPosition(

    @SerializedName("Latitude") var latitude: Double? = null,
    @SerializedName("Longitude") var longitude: Double? = null
)

data class Response(
    @SerializedName("MetaInfo") var metaInfo: MetaInfo? = null,
    @SerializedName("View") var view: List<View>? = null
)

data class Result (
    @SerializedName("Relevance") var relevance: Int? = null,
    @SerializedName("Distance") var distance: Double? = null,
    @SerializedName("MatchLevel") var matchLevel: String? = null,
    @SerializedName("MatchQuality") var matchQuality: MatchQuality? = null,
    @SerializedName("MatchType") var matchType: String? = null,
    @SerializedName("Location") var location: Location? = null
)

data class TopLeft(
    @SerializedName("Latitude") var latitude: Double? = null,
    @SerializedName("Longitude") var longitude: Double? = null
)

data class View(
    @SerializedName("_type") var type: String? = null,
    @SerializedName("ViewId") var viewId: Int? = null,
    @SerializedName("Result") var result: List<Result>? = null

)