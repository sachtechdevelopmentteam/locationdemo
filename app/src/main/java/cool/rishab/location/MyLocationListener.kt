package cool.rishab.location


import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Response

open class MyLocationListener(val activity: Activity, var googleMap: GoogleMap?) {

    private val UPDATE_INTERVAL = 2000
    private val FASTEST_INTERVAL = 1500
    private val LOCATION_REQUEST_PERMISSION = 100
    private val LOCATION_REQUEST_CODE = 200
    var mLastKnownLocation: Location? = null
    private lateinit var locationCallback: LocationCallback
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null


    //initialize
    init {
        //check permissions for accessing fine and coarse location
        if (checkPermissions(
                arrayOf(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
        )
        //if available it checks the gps is off or on
            turnOnGps()
        else
        //request permissions if it is not enabled on device
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(
                    arrayOf(
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ), LOCATION_REQUEST_PERMISSION
                )
            }
    }

    //if gps is disable it enables the gps and gives the result in onActivityResult
    private fun turnOnGps() {
        val manager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            createGoogleApi()
        } else if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !manager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
            )
        )
            buildAlertMessageNoGps()
    }

    //for enable the gps
    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            .setCancelable(false)
            .setPositiveButton(
                "Yes"
            ) { _dialog, id_ ->
                activity.startActivityForResult(
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                    LOCATION_REQUEST_CODE
                )
            }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    //getting fused location
    private fun createGoogleApi() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)
        getDeviceLastLocation()
    }


    fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
    }

    //getting location by device
    @SuppressLint("MissingPermission")
    fun getDeviceLastLocation() {
        val locationResult = mFusedLocationProviderClient?.lastLocation;
        locationResult?.addOnCompleteListener {
            if (it.isSuccessful) {
                mLastKnownLocation = it.result;
                if (mLastKnownLocation == null)
                    startLocationUpdates()
                else
                    onLocationUpdate {
                        val latLng = LatLng(mLastKnownLocation?.latitude!!, mLastKnownLocation?.longitude!!)
                        getAddressFromHereApi(latLng)
                    }
            } else {
                startLocationUpdates()
            }
        }
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        var mLocationRequest = LocationRequest();
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = UPDATE_INTERVAL.toLong()
        mLocationRequest.fastestInterval = FASTEST_INTERVAL.toLong()

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(activity)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                if (p0 != null) {
                    val lastLocation = p0.lastLocation
                    if (lastLocation != null) {
                        removeLocationUpdates()
                        onLocationUpdate {
                            val latLng = LatLng(lastLocation.latitude, lastLocation.longitude)
                            getAddressFromHereApi(latLng)
                        }
                    }
                }
            }

            override fun onLocationAvailability(p0: LocationAvailability?) {
                super.onLocationAvailability(p0)
                if (p0?.isLocationAvailable == false)
                    getDeviceLastLocation()
            }
        }
        mFusedLocationProviderClient?.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper())
    }

    fun removeLocationUpdates() {
        mFusedLocationProviderClient?.removeLocationUpdates(locationCallback)
    }

    //set filter on place autocomplete fragment
    fun setPlaceAutocompleteFragment(placefragment: PlaceAutocompleteFragment?) {
        val typeFilter = AutocompleteFilter.Builder()
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).setCountry("IN").build()
        placefragment?.setFilter(typeFilter)
        placefragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {

            override fun onPlaceSelected(p0: Place?) {
                getAddressFromHereApi(p0?.latLng!!)
            }

            override fun onError(p0: Status?) {
                toast(p0.toString())
            }
        })
    }

    //get address from geocoding using here api
    fun getAddressFromHereApi(latLng: LatLng) {
        val latlng = "${latLng.latitude},${latLng.longitude}"
        val here_app_id = activity.resources.getString(R.string.here_map_app_id)
        val here_app_code = activity.resources.getString(R.string.here_map_app_code)
        apiHitter().getLocation(latlng, "retrieveAddresses", here_app_id, here_app_code)
            .enqueue(object : retrofit2.Callback<LocationResponse> {
                override fun onFailure(call: Call<LocationResponse>, t: Throwable) {
                   toast(t.localizedMessage)
                }
                override fun onResponse(call: Call<LocationResponse>, response: Response<LocationResponse>) {
                    val addresslist = response.body()?.response?.view?.get(0)?.result?.get(0)?.location?.address
                    val position = response.body()?.response?.view?.get(0)?.result?.get(0)?.location?.displayPosition
                    val address = addresslist?.label
                    val latitude = position?.latitude
                    val longitude = position?.longitude
                    val latLng = LatLng(latitude!!, longitude!!)
                    toast("$address")
                    showMarker(latLng, googleMap)
                }
            })
    }

    //show marker on googlemap using latlng
    private fun showMarker(latLng: LatLng?, googleMap: GoogleMap?) {
        googleMap?.addMarker(MarkerOptions().position(latLng!!).visible(true))
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        googleMap?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    //requested permission result
    fun onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == LOCATION_REQUEST_PERMISSION) {
            createGoogleApi()
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == LOCATION_REQUEST_CODE)
            createGoogleApi()
    }

    //called when location is updated
    inline fun onLocationUpdate(location: () -> Unit) {
        location()
    }

    fun toast(msg:String)
    {
        Toast.makeText(activity,msg, Toast.LENGTH_SHORT).show()
    }
}

