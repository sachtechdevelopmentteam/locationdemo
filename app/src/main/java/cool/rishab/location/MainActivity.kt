package cool.rishab.location

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment


class MainActivity : AppCompatActivity(), OnMapReadyCallback{
    var placefragment: PlaceAutocompleteFragment? = null
    var googleMap: GoogleMap? = null
    lateinit var mapFragment: SupportMapFragment
    lateinit var myLocationListener: MyLocationListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        myLocationListener = MyLocationListener(this,googleMap)
        initializeMapFragment()
        initializePlaceFragment()
    }

    private fun initializeMapFragment() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun initializePlaceFragment() {
        placefragment = fragmentManager?.findFragmentById(R.id.place_autocomplete) as? PlaceAutocompleteFragment
        myLocationListener.setPlaceAutocompleteFragment(placefragment)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
       myLocationListener.onMapReady(googleMap)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        myLocationListener.onRequestPermissionResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        myLocationListener.onActivityResult(requestCode, resultCode, data)
    }
}
