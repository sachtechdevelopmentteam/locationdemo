package cool.rishab.location

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("reversegeocode.json")
    fun getLocation(
        @Query("prox") prox: String,//latitude,longitude
        @Query("mode") mode: String = "retrieveAddresses",
        @Query("app_id") app_id: String,//here_map_app_id
        @Query("app_code") app_code: String//here_map_app_code
    ): Call<LocationResponse>
}
